# fCC-tutorials

This is a collection of my implementations of freeCodeCamp.org courses involving the rust programming language

## Courses

Courses used in this collection:

[Rust Programming Language Tutorial - How to Build a To-Do List App](https://www.freecodecamp.org/news/how-to-build-a-to-do-app-with-rust/)
