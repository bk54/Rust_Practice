use std::collections::HashMap;
// below needed only for txt file implementation
//use std::io::Read;
//use std::str::FromStr;

struct Todo {
    map: HashMap<String, bool>,
}
impl Todo {
    // reads file (db.json) and populates Todo with any data
    fn new() -> Result<Todo, std::io::Error> {
        let file = std::fs::OpenOptions::new() // let mut file -if using txt file
            .write(true)
            .create(true)
            .read(true)
            .open("db.json")?;

        // This section is designed to create txt file
        //let mut content = String::new();
        //file.read_to_string(&mut content)?;
        //let map: HashMap<String, bool> = content
        //  .lines()
        //  .map(|line| line.splitn(2, '\t').collect::<Vec<&str>>())
        //  .map(|vector| (vector[0], vector[1]))
        //  .map(|(key, value)| (String::from(key), bool::from_str(value).unwrap()))
        //  .collect();
        //Ok(Todo {map})

        match serde_json::from_reader(file) {
            Ok(map) => Ok(Todo {map}),
            Err(e) if e.is_eof() => Ok(Todo {
                map: HashMap::new(),
            }),
            Err(e) => panic!("An error occurred: {}", e),
        }
    }

    // inserts a new item into the Todo map
    fn insert(&mut self, key: String) {
        self.map.insert(key, true);
    }

    // writing Todo map into a txt file (db.txt) to disk
    //fn save(self) -> Result<(), std::io::Error> {
    //    let mut content = String::new();
    //    for (key, value) in self.map {
    //        let record = format!("{}\t{}\n", key, value);
    //        content.push_str(&record)
    //    }
    //    std::fs::write("db.txt", content)
    //}

    // writing Todo map into a json file (db.json)
    fn save(self) -> Result<(), Box<dyn std::error::Error>> {
        let file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open("db.json")?;
        serde_json::to_writer_pretty(file, &self.map)?;
        Ok(())
    }

    // updates value of todo list item to completed (setting it to false)
    fn complete(&mut self, key: &String) -> Option<()> {
        match self.map.get_mut(key) {
            Some(value) => Some(*value = false),
            None => None,
        }
    }
}

fn main() {
    let action = std::env::args().nth(1).expect("Please specify an action\nOptions:\nadd\ncomplete");
    let item = std::env::args().nth(2).expect("Please specify an item");

    println!("{:?}, {:?}", action, item);

    let mut todo = Todo::new().expect("Initialisation of db failed");
    if action == "add" {
        todo.insert(item);
        match todo.save() {
            Ok(_) => println!("todo saved"),
            Err(why) => println!("An error occured: {}", why),
        }
    }
    else if action == "complete" {
        match todo.complete(&item) {
            None => println!("'{}' is not present in the list", item),
            Some(_) => match todo.save() {
                Ok(_) => println!("todo saved"),
                Err(why) => println!("An error occurred: {}", why),
            },
        }
    }
}
